import pandas as pd
import xml.etree.ElementTree as ET

class XMLParser:
    def __init__(self, xml_file_path, csv_file_path):
        self.xml_file_path = xml_file_path
        self.csv_file_path = csv_file_path
        
    def parse_xml(self):
        # Parse the XML file
        tree = ET.parse(self.xml_file_path)
        root = tree.getroot()
        
        # Initialize the generated ID counter and data list
        id_counter = 1
        data = []
        
        # Loop through each <person> element
        for person in root.findall('person'):
            # Get the first name and surname elements
            first_name = person.find('firstName').text.strip()
            surname = person.find('surname').text.strip()
            
            # Reverse the first name and capitalize the first letter
            reversed_first_name = first_name[::-1].capitalize()
            
            # Add a dictionary of data to the list
            data.append({'generated id': id_counter, 'first name': reversed_first_name, 'surname': surname})
            
            # Increment the generated ID counter
            id_counter += 1
        
        # Create a pandas DataFrame from the data list
        df = pd.DataFrame(data)
        
        # Save the DataFrame to a CSV file
        df.to_csv(self.csv_file_path, index=False, columns=['generated id', 'first name', 'surname'])

# Create an instance of the XMLParser class
parser = XMLParser('path/to/xml/file.xml', 'path/to/csv/file.csv')

# Call the parse_xml method to read the XML file and save the output to a CSV file
parser.parse_xml()
