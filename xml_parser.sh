#!/bin/bash

# Check if both input and output file paths are provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 input_file_path output_file_path"
    exit 1
fi

# Check if the input file exists
if [ ! -f "$1" ]; then
    echo "Input file does not exist: $1"
    exit 1
fi

# Call the Python script to parse the XML file and save the output to a CSV file
python3 xml_parser.py "$1" "$2"

# Check if the output file was created successfully
if [ ! -f "$2" ]; then
    echo "Failed to create output file: $2"
    exit 1
fi

# Print success message
echo "Output file created successfully: $2"
