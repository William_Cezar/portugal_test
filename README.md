# Code challenge

1 - Create a Python class that reads the given XML from a file location and returns all the first names reversed with the first letter capitalized followed by the surname. The output should be saved to another file location as a csv with columns “generated id”, “first name”, “surname”.

2 - Create a bash script that takes the following arguments:
- a file location for the input file
- a file location for the output file

And passes them to the previously created Python script with error handling for invalid inputs